package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * 
 *
 */
public final class AnotherConcurrentGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final Agent agent;

    /**
     * 
     */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);

        agent = new Agent();
        new Thread(agent).start();
        new Thread(new TimeAgent()).start();

        stop.addActionListener(new ActionListener() {
            /**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.stop.setEnabled(false);
                            AnotherConcurrentGUI.this.up.setEnabled(false);
                            AnotherConcurrentGUI.this.down.setEnabled(false);
                        }
                    });
                } catch (InvocationTargetException | InterruptedException e1) {
                    e1.printStackTrace();
                }

            }
        });

        up.addActionListener(e -> agent.changeDirection());

        down.addActionListener(e-> agent.changeDirection());

    }

    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private int counter;
        private volatile boolean direction = true; //true UP false DOWN

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });

                    if (direction) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        /**
         * Excternal command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }

        public void changeDirection() {
            this.direction = this.direction ? false : true;
        }
    }

    private class TimeAgent implements Runnable {
        private static final long TIME = 10000;

        @Override
        public void run() {
            try {
                Thread.sleep(TIME);
                SwingUtilities.invokeAndWait(new Runnable() {

                    @Override
                    public void run() {
                        AnotherConcurrentGUI.this.agent.stopCounting();
                        AnotherConcurrentGUI.this.up.setEnabled(false);
                        AnotherConcurrentGUI.this.down.setEnabled(false);
                        AnotherConcurrentGUI.this.stop.setEnabled(false);
                    }
                });

            } catch (InterruptedException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

    }
}