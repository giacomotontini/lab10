package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 */
public class MultiThreadedSumMatrix implements SumMatrix {

    private final int nthread;

    /**
     * 
     * @param nthread 
     */
    public MultiThreadedSumMatrix(final int nthread) {
        super();
        this.nthread = nthread;
    }

    private static final class Worker extends Thread {
        private final double[][] matrix;
        private final int startrow;
        private final int nrow;
        private long res;
        private final int colonne;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startrow, final int nrow) {
            super();
            this.matrix = matrix;
            this.startrow = startrow;
            this.nrow = nrow;
            colonne = matrix[0].length;
        }

        @Override
        public void run() {
            System.out.println("Working from row " + startrow + " to row " + (startrow + nrow));
            for (int k = startrow; k < startrow + nrow && k < matrix.length; k++) {
                for (int i = 0; i < colonne; i++) {
                    res += matrix[k][i];
                }
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }

    }

    @Override
    public double sum(final double[][] matrix) {

        final int nrow = matrix.length / nthread + matrix.length % nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = IntStream.iterate(0, start -> start + nrow)
                .limit(nthread)
                .mapToObj(start -> new Worker(matrix, start, nrow))
                .collect(Collectors.toList());
        /*
         * Start them
         */
        workers.forEach(Thread::start);
        /*
         * Wait for every one of them to finish
         */
        workers.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        /*
         * Return the sum
         */
        return workers.stream().mapToLong(Worker::getResult).sum();
    }
}
